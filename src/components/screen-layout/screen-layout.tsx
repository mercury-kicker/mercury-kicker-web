import Head from "next/head";
import styles from "./screen-layout.module.css";
import React from "react";
import classNames from "classnames/bind";
import { ScreenHeader } from "./screen-header";

const cx = classNames.bind(styles);

interface Props {
  title: string;
  children: React.ReactNode;
}

export const ScreenLayout = (props: Props) => {
  return (
    <div>
      <Head>
        <title>{props.title}</title>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <div className={cx("screen-layout__header")}>
        <ScreenHeader />
      </div>
      {props.children}
    </div>
  );
};
