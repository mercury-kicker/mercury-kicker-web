import styles from "./sign-in.module.css";
import { signIn } from "next-auth/client";
import React from "react";
import classNames from "classnames/bind";

const cx = classNames.bind(styles);

export const SignIn = (props: JSX.IntrinsicElements["div"]) => {
  return (
    <div {...props}>
      <button
        className={cx("sign-in")}
        onClick={() => {
          signIn("google");
        }}
      >
        Войти
      </button>
    </div>
  );
};
