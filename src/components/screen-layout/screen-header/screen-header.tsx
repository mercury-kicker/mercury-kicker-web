import Link from "next/link";
import React from "react";
import { useSession } from "next-auth/client";
import classNames from "classnames/bind";
import styles from "./screen-header.module.css";
import { Logo } from "src/components/logo";
import { SignIn } from "./sign-in";
import { UserInfo } from "./user-info";
import { SignOut } from "./sign-out";

const cx = classNames.bind(styles);

export const ScreenHeader = () => {
  const [session, loading] = useSession();

  return (
    <div className={cx("screen-header")}>
      <Link href="/" passHref>
        <a>
          <Logo className={cx("screen-header__logo")} />
        </a>
      </Link>
      {!session && <SignIn className={cx("screen-header__sign-in")} />}
      {session?.user && (
        <>
          <UserInfo
            session={session}
            className={cx("screen-header__user-info")}
          />
          <SignOut className={cx("screen-header__sign-out")} />
        </>
      )}
    </div>
  );
};
