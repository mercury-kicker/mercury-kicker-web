import { Session } from "next-auth";
import styles from "./user-info.module.css";
import React from "react";
import classNames from "classnames/bind";

const cx = classNames.bind(styles);

interface Props {
  session: Session | null;
}

export const UserInfo = (props: Props & JSX.IntrinsicElements["div"]) => {
  return (
    <div {...props}>
      <span className={cx("user-info")}>
        {props?.session?.user?.image && (
          <img
            className={cx("user-info__image")}
            src={props?.session?.user?.image}
            alt={
              (props?.session?.user?.email || props?.session?.user?.name) ?? ""
            }
          />
        )}
        <strong className={cx("user-info__name")}>
          {props?.session?.user?.name}
        </strong>
      </span>
    </div>
  );
};
