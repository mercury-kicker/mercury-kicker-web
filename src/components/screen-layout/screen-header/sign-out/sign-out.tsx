import styles from "./sign-out.module.css";
import { signOut } from "next-auth/client";
import React from "react";
import classNames from "classnames/bind";

const cx = classNames.bind(styles);

export const SignOut = (props: JSX.IntrinsicElements["div"]) => {
  return (
    <div {...props}>
      <button
        className={cx("sign-out")}
        onClick={() => {
          signOut();
        }}
      >
        Выйти
      </button>
    </div>
  );
};
