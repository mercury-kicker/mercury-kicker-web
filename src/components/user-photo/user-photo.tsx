import styles from "./user-photo.module.css";
import React from "react";
import classNames from "classnames/bind";

const cx = classNames.bind(styles);

interface Props {
  index: number;
  photoUrl: string;
}

export const UserPhoto = (props: Props) => {
  return (
    <div className={cx("user-photo")}>
      <div className={cx("user-photo__number")}>
        <label className={cx("user-photo__label")}>{props.index}</label>
      </div>
      <img className={cx("user-photo__image")} src={props.photoUrl} alt="" />
    </div>
  );
};
