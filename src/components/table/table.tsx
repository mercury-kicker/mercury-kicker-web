import React from "react";
import styles from "./table.module.css";
import classNames from "classnames/bind";
import {
  Cell,
  Column,
  ColumnInstance,
  HeaderGroup,
  Row,
  TableCellProps,
  TableCommonProps,
  TableHeaderProps,
  TableRowProps,
  useTable,
} from "react-table";

const cx = classNames.bind(styles);

type Props<D extends object> = JSX.IntrinsicElements["table"] & {
  columns: Column<D>[];
  data: D[];
  getHeaderProps?: (headerGroup: HeaderGroup<D>) => Partial<TableHeaderProps>;
  getColumnProps?: (column: ColumnInstance<D>) => Partial<TableCommonProps>;
  getRowProps?: (row: Row<D>) => Partial<TableRowProps>;
  getCellProps?: (cell: Cell<D>) => Partial<TableCellProps>;
};

const defaultPropGetter = () => ({});

export const Table = <D extends object = {}>({
  columns,
  data,
  getHeaderProps = defaultPropGetter,
  getColumnProps = defaultPropGetter,
  getRowProps = defaultPropGetter,
  getCellProps = defaultPropGetter,
  ...tableProps
}: Props<D>) => {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable<D>({
      columns,
      data,
    });

  return (
    <table className={cx("table")} {...getTableProps()} {...tableProps}>
      <thead>
        {headerGroups.map((headerGroup) => {
          const { key, ...restHeaderGroupProps } =
            headerGroup.getHeaderGroupProps();
          return (
            <tr
              key={key}
              className={cx("table__head")}
              {...restHeaderGroupProps}
            >
              {headerGroup.headers.map((column) => {
                const { key, className, ...restColumnProps } =
                  column.getHeaderProps((props) => [
                    props,
                    getColumnProps(column),
                    getHeaderProps(column),
                  ]);
                return (
                  <th
                    key={key}
                    className={cx("table__cell_header", className)}
                    {...restColumnProps}
                  >
                    {column.render("Header")}
                  </th>
                );
              })}
            </tr>
          );
        })}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row) => {
          prepareRow(row);
          const { key, className, ...restRowProps } = row.getRowProps(
            (props) => [props, getRowProps(row)]
          );

          return (
            <tr
              key={key}
              className={cx("table__row", className)}
              {...restRowProps}
            >
              {row.cells.map((cell) => {
                const { key, className, ...restCellProps } = cell.getCellProps(
                  (props) => [
                    props,
                    getColumnProps(cell.column),
                    getCellProps(cell),
                  ]
                );
                return (
                  <td
                    key={key}
                    className={cx("table__cell", className)}
                    {...restCellProps}
                  >
                    {cell.render("Cell")}
                  </td>
                );
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};
