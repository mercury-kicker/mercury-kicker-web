import React from "react";
import { Team } from "src/types";
import styles from "./team-photo.module.css";
import classNames from "classnames/bind";

const cx = classNames.bind(styles);
interface Props {
  position?: number;
  team: Team;
  style?: React.CSSProperties;
}

export const TeamPhoto: React.FC<Props> = (props) => {
  const { position, team, ...otherProps } = props;

  return (
    <div className={cx("team-photo")} {...otherProps}>
      {position ? (
        <div className={cx("team-photo__position-number", "position-number")}>
          <label className={cx("position-number__text")}>{position}</label>
        </div>
      ) : null}
      <img
        className={cx(
          "team-photo__image",
          "team-photo__image_position_defender"
        )}
        alt="Player Photo"
        src={team && team.player1.photoUrl}
      />
      <img
        className={cx(
          "team-photo__image",
          "team-photo__image_position_forward"
        )}
        alt="Player Photo"
        src={team && team.player2.photoUrl}
      />
    </div>
  );
};
