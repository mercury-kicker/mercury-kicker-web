import { PlayerPosition } from "src/types";

export const isPlayerPosition = (
  queryPlayer: any
): queryPlayer is PlayerPosition => {
  const playerNumber = Number(queryPlayer);
  return Object.values(PlayerPosition).includes(playerNumber as PlayerPosition);
};
