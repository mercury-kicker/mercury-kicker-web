import { TeamPosition } from "src/types";

export const isTeamPosition = (queryTeam: any): queryTeam is TeamPosition => {
  const teamNumber = Number(queryTeam);
  return Object.values(TeamPosition).includes(teamNumber as TeamPosition);
};
