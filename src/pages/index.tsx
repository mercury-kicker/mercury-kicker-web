import { Home } from "src/screens/home";
import { GetServerSideProps, NextPage } from "next";
import { mutate } from "swr";
import { fetcher } from "src/lib/fetcher";
import { apiRoutesFetcher } from "src/lib/api-routes-fetcher";

export const getServerSideProps: GetServerSideProps = async () => {
  const tournamentResponse = await fetcher("/tournaments/last");
  const tournamentStatsResponse = await fetcher(
    `/tournaments/${tournamentResponse.tournament.id}/stats`
  );
  const tournamentGamesResults = await fetcher(
    `/tournaments/${tournamentResponse.tournament.id}/games-results`
  );
  const usersResponse = await apiRoutesFetcher("/users");

  const prefetchedData = {
    ["/tournaments/last"]: tournamentResponse,
    [`/tournaments/${tournamentResponse.tournament.id}/stats`]:
      tournamentStatsResponse,
    [`/tournaments/${tournamentResponse.tournament.id}/games-results`]:
      tournamentGamesResults,
    ["/users"]: usersResponse,
  };

  return {
    props: {
      prefetchedData,
    },
  };
};

interface Props {
  prefetchedData?: any;
}

const HomePage: NextPage<Props> = ({ prefetchedData }) => {
  for (let key in prefetchedData) {
    mutate(key, prefetchedData[key]);
  }

  return <Home />;
};

export default HomePage;
