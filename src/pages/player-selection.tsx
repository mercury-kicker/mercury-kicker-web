import { GetServerSideProps } from "next";
import { getSession } from "next-auth/client";
import Head from "next/head";
import { fetcher } from "src/lib/fetcher";
import { PlayerSelection } from "src/screens/player-selection";
import { User } from "src/types";

export const getServerSideProps: GetServerSideProps = async (req) => {
  const session = await getSession(req);

  // @TODO: link authentication flow with the users db
  const usersResponse = await fetcher("/users");
  const user = usersResponse.users?.find(
    (user: any) => user?.email === session?.user?.email
  );

  return {
    props: { user: user ?? null },
  };
};

interface Props {
  user: User | null;
}

export default function PlayerSelectionPage({ user }: Props) {
  return (
    <>
      <Head>
        <title>Player selection</title>
      </Head>
      <PlayerSelection user={user} />
    </>
  );
}
