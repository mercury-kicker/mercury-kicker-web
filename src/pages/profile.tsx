import { GetServerSideProps } from "next";
import { getSession } from "next-auth/client";
import { Profile } from "src/screens/profile";

export const getServerSideProps: GetServerSideProps = async (req) => {
  const session = await getSession(req);
  const user = session?.user;

  if (!user) {
    return {
      redirect: {
        destination: "/sign-in",
        permanent: false,
      },
    };
  }

  return {
    props: { user },
  };
};

export default function ProfilePage() {
  return <Profile />;
}
