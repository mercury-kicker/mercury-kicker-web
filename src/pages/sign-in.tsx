import { GetServerSideProps } from "next";
import { getSession } from "next-auth/client";
import { SignIn } from "src/screens/sign-in";

export const getServerSideProps: GetServerSideProps = async (req) => {
  const session = await getSession(req);
  const user = session?.user;

  if (user) {
    return {
      redirect: {
        destination: "/profile",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

export default function SignInPage() {
  return <SignIn />;
}
