import { NextPage, GetServerSideProps } from "next";
import { mutate } from "swr";
import { Tournaments } from "src/screens/tournaments";
import { apiRoutesFetcher } from "src/lib/api-routes-fetcher";

export const getServerSideProps: GetServerSideProps = async () => {
  const tournamentsSummaryResponse = await apiRoutesFetcher(
    "/tournaments-summary"
  );

  const prefetchedData = {
    ["/tournaments-summary"]: tournamentsSummaryResponse,
  };

  return {
    props: {
      prefetchedData,
    },
  };
};

interface Props {
  prefetchedData?: any;
}

const TournamentsPage: NextPage<Props> = ({ prefetchedData }) => {
  for (let key in prefetchedData) {
    mutate(key, prefetchedData[key]);
  }

  return <Tournaments />;
};

export default TournamentsPage;
