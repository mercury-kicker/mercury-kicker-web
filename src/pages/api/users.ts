import type { NextApiRequest, NextApiResponse } from "next";
import { fetcher } from "src/lib/fetcher";
import { User } from "src/types";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<User[]>
) {
  const { users } = await fetcher("/users");
  users.forEach((user: User) => delete user.email);
  res.status(200).send(users);
}
