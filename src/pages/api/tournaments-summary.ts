import type { NextApiRequest, NextApiResponse } from "next";
import { fetcher } from "src/lib/fetcher";
import {
  TeamStats,
  Tournament,
  TournamentStats,
  TournamentSummary,
} from "src/types";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<TournamentSummary[]>
) {
  const tournamentsResponse = await fetcher("/tournaments/");
  const tournaments = tournamentsResponse.tournaments;

  const statsRequests = tournaments.map((tournament: Tournament) =>
    fetcher(`/tournaments/${tournament.id}/stats`)
  );

  const statsResponses: TournamentStats[] = await Promise.all(statsRequests);

  statsResponses.forEach((statsResponse) =>
    statsResponse.stats.sort((a: TeamStats, b: TeamStats) => b.wins - a.wins)
  );

  const winners = statsResponses.map(
    (statsResponse) => statsResponse.stats[0]?.team ?? null
  );

  const tournamentsSummary = tournaments.map(
    (tournament: TournamentSummary, index: number) => {
      return {
        id: tournament.id,
        title: tournament.title,
        startDate: tournament.startDate,
        endDate: tournament.endDate,
        winnerTeam: winners[index],
      };
    }
  );

  res.status(200).send(tournamentsSummary);
}
