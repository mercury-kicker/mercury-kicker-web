import useSWR from "swr";
import { fetcher } from "src/lib/fetcher";
import { Tournament, GameResults } from "src/types";

export function useTournamentGamesResults(id: Tournament["id"]) {
  return useSWR<{ gamesResults: GameResults[] }>(
    `/tournaments/${id}/games-results`,
    fetcher
  );
}
