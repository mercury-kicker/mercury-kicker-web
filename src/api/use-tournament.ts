import useSWR from "swr";
import { fetcher } from "src/lib/fetcher";
import { Tournament } from "src/types";

export function useTournament() {
  return useSWR<{ tournament: Tournament }>("/tournaments/last", fetcher);
}
