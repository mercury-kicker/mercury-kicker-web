import useSWR from "swr";
import { apiRoutesFetcher } from "src/lib/api-routes-fetcher";
import { User } from "src/types";

export function useUsers() {
  return useSWR<User[]>("/users", apiRoutesFetcher);
}
