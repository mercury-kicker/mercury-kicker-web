import useSWR from "swr";
import { fetcher } from "src/lib/fetcher";
import { TournamentStats, Tournament } from "src/types";

export function useTournamentStats(id: Tournament["id"]) {
  return useSWR<TournamentStats>(`/tournaments/${id}/stats`, fetcher);
}
