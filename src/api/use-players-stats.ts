import useSWR from "swr";
import { fetcher } from "src/lib/fetcher";
import { User, PlayerStats } from "src/types";

type PlayerStatsParams = {
  playerId?: User["id"];
};

export function usePlayerStats({ playerId }: PlayerStatsParams) {
  const searchParams = new URLSearchParams();
  if (playerId) {
    searchParams.append("userId", playerId.toString());
  }

  return useSWR<{
    usersStats: {
      all: PlayerStats[];
      forwards: PlayerStats[];
      defenders: PlayerStats[];
    };
  }>(`/stats?${searchParams.toString()}`, fetcher);
}
