import axios from "axios";
import { User, GameSlot } from "src/types";

const baseURL = process.env.NEXT_PUBLIC_API_HOST;

export function postSelectPlayerEvent(payload: GameSlot) {
  return axios.post(`${baseURL}/api/active-game/events`, {
    type: "select-player",
    payload,
  });
}
