import useSWR from "swr";
import { apiRoutesFetcher } from "src/lib/api-routes-fetcher";
import { TournamentSummary } from "src/types";

export function useTournamentsSummary() {
  return useSWR<TournamentSummary[]>("/tournaments-summary", apiRoutesFetcher);
}
