import axios from "axios";

const instance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_ROUTES + "/api",
});

export const apiRoutesFetcher = async (url) => {
  const res = await instance.get(url);
  return res.data;
};
