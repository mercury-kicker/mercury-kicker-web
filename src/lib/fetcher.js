import axios from "axios";

const instance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_HOST + "/api",
});

export const fetcher = async (url) => {
  const res = await instance.get(url);
  return res.data;
};
