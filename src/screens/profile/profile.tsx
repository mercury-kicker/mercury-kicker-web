import { GetServerSideProps } from "next";
import { getSession, signOut, useSession } from "next-auth/client";
import styles from "./profile.module.css";
import { ScreenLayout } from "src/components/screen-layout";
import classNames from "classnames/bind";

const cx = classNames.bind(styles);

export const getServerSideProps: GetServerSideProps = async (req) => {
  const session = await getSession(req);
  const user = session?.user;

  if (!user) {
    return {
      redirect: {
        destination: "/sign-in",
        permanent: false,
      },
    };
  }

  return {
    props: { user },
  };
};

export function Profile() {
  const [session, loading] = useSession();

  return (
    <ScreenLayout title="Profile">
      <div className={cx("profile")}>
        {session?.user && (
          <>
            <span>
              <small className={cx("profile__text")}>Signed in as</small>
              <br />
              {session.user.image && (
                <img
                  src={session.user.image}
                  alt={(session.user.email || session.user.name) ?? ""}
                />
              )}
              <br />
              <strong className={cx("profile__text")}>
                {session.user.email || session.user.name}
              </strong>
            </span>
            <br />
            <br />
            <a
              href={`/api/auth/signout`}
              className={cx("profile__text")}
              onClick={(e) => {
                e.preventDefault();
                signOut();
              }}
            >
              Sign out
            </a>
          </>
        )}
      </div>
    </ScreenLayout>
  );
}
