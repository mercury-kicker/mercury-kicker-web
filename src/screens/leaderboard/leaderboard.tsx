import styles from "./leaderboard.module.css";
import React, { useCallback } from "react";
import { Cell, Column, ColumnInstance, TableCommonProps } from "react-table";
import classNames from "classnames/bind";
import { usePlayerStats } from "src/api/use-players-stats";
import { PlayerStats } from "src/types";
import { ScreenLayout } from "src/components/screen-layout";
import { UserPhoto } from "src/components/user-photo";
import { Table } from "src/components/table";

const cx = classNames.bind(styles);

export const Leaderboard = () => {
  const { data, error } = usePlayerStats({});

  const columns = React.useMemo(
    (): Column<PlayerStats>[] => [
      {
        id: "user.photo",
        Header: "Player",
        accessor: "photoUrl",
        Cell(cell: Cell) {
          return <UserPhoto index={cell.row.index + 1} photoUrl={cell.value} />;
        },
      },
      {
        id: "user.name",
        Header: "",
        accessor: "name",
        Cell(cell: Cell) {
          return (
            <div className={cx("leaderboard__user-name")}>{cell.value}</div>
          );
        },
      },
      {
        id: "user.games",
        Header: "Games",
        accessor: "games",
      },
      {
        id: "user.wins",
        Header: "Wins %",
        accessor: "wins",
      },
      {
        id: "user.goals",
        Header: "Goals avg.",
        accessor: "goals",
      },
      {
        id: "user.keep",
        Header: "Keeps avg.",
        accessor: "keep",
      },
      {
        id: "user.rating",
        Header: "Rating",
        accessor: "rating",
      },
    ],
    []
  );

  const stats = React.useMemo(() => {
    if (data?.usersStats?.all) {
      return data.usersStats.all
        .slice()
        .sort((user1: PlayerStats, user2: PlayerStats) =>
          user1.rating < user2.rating ? 1 : -1
        );
    }
    return [];
  }, [data?.usersStats?.all]);

  const getColumnProps = useCallback(
    (column: ColumnInstance<PlayerStats>): Partial<TableCommonProps> => {
      if (column.id === "user.photo") {
        return { style: { fontWeight: "bolder" } };
      }
      if (column.id === "user.name") {
        return { className: cx("leaderboard__cell_player") };
      }
      if (column.id === "user.games") {
        return { className: cx("leaderboard__cell_results") };
      }
      if (column.id === "user.wins") {
        return { className: cx("leaderboard__cell_results") };
      }
      if (column.id === "user.goals") {
        return { className: cx("leaderboard__cell_results") };
      }
      if (column.id === "user.keep") {
        return { className: cx("leaderboard__cell_results") };
      }
      if (column.id === "user.rating") {
        return {
          className: cx(
            "leaderboard__cell_results",
            "leaderboard__cell_rating"
          ),
        };
      }
      return {};
    },
    []
  );

  return (
    <ScreenLayout title="Leaderboard">
      <div className={cx("leaderboard")}>
        <p className={cx("leaderboard__title")}>Рейтинг игроков</p>
        <Table
          className={cx("leaderboard__table")}
          data={stats}
          columns={columns}
          getColumnProps={getColumnProps}
        />
      </div>
    </ScreenLayout>
  );
};
