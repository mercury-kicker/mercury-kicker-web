import { signIn, useSession } from "next-auth/client";
import { ScreenLayout } from "src/components/screen-layout";
import styles from "./sign-in.module.css";
import classNames from "classnames/bind";

const cx = classNames.bind(styles);

export function SignIn() {
  const [session] = useSession();

  return (
    <ScreenLayout title="Sign In">
      <div className={cx("sign-in")}>
        {!session && (
          <>
            <span className={cx("sign-in__text")}>You are not signed in</span>
            <br />
            <a
              className={cx("sign-in__text")}
              href={`/api/auth/signin`}
              onClick={(e) => {
                e.preventDefault();
                signIn();
              }}
            >
              Sign in with Google
            </a>
          </>
        )}
      </div>
    </ScreenLayout>
  );
}
