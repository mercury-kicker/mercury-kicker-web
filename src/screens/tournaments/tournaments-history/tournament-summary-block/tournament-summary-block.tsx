import React from "react";
import styles from "./tournament-summary-block.module.css";
import format from "date-fns/format";
import classNames from "classnames/bind";
import { TeamPhoto } from "src/components/team-photo";
import { TournamentSummary } from "src/types";

let cx = classNames.bind(styles);

interface Props {
  tournamentSummary: TournamentSummary;
}

export const TournamentSummaryBlock: React.FC<Props> = ({
  tournamentSummary,
}) => {
  return (
    <div className={cx("tournament-summary")}>
      <p className={cx("tournament-summary__date")}>
        {format(tournamentSummary.startDate, "DD MMM")}
        {" — "}
        {format(tournamentSummary.endDate, "DD MMM YYYY")}
      </p>
      <p className={cx("tournament-summary__title")}>
        {tournamentSummary.title}
      </p>

      <div className={cx("tournament-summary__team-winner", "team-winner")}>
        {tournamentSummary.winnerTeam ? (
          <>
            <div className={cx("team_winner__team-info")}>
              <TeamPhoto team={tournamentSummary.winnerTeam} />
              <div>
                <p className={cx("team-winner__name")}>
                  {tournamentSummary.winnerTeam?.name}
                </p>
                <p className={cx("team-winner__players-names")}>
                  {tournamentSummary.winnerTeam?.player1.name}
                  {", "}
                  <br className={cx("team-winner__break")} />
                  {tournamentSummary.winnerTeam?.player2.name}
                </p>
              </div>
            </div>
            <div className={cx("team-winner__award")}></div>
          </>
        ) : (
          <p className={cx("team-winner__placeholder")}>No winner</p>
        )}
      </div>
    </div>
  );
};
