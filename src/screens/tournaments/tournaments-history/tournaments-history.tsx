import React from "react";
import styles from "./tournaments-history.module.css";
import classNames from "classnames/bind";
import { TournamentSummaryBlock } from "./tournament-summary-block";
import { useTournamentsSummary } from "src/api/use-tournaments-summary";

let cx = classNames.bind(styles);

export const TournamentsHistory: React.FC = (
  props: JSX.IntrinsicElements["div"]
) => {
  const { data: tournamentsSummary, error } = useTournamentsSummary();

  if (error) return <div>An error has occurred.</div>;
  if (!tournamentsSummary && !error) return <div>Loading...</div>;

  return (
    <div {...props}>
      <p className={cx("tournaments-history__label")}>История турниров</p>
      <div className={cx("tournaments-history__blocks")}>
        {tournamentsSummary &&
          tournamentsSummary.map((tournamentSummary) => (
            <TournamentSummaryBlock
              tournamentSummary={tournamentSummary}
              key={tournamentSummary.id}
            />
          ))}
      </div>
    </div>
  );
};
