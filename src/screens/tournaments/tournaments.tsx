import styles from "./tournaments.module.css";
import classNames from "classnames/bind";
import { ScreenLayout } from "src/components/screen-layout";
import { TournamentsHistory } from "./tournaments-history";

let cx = classNames.bind(styles);

export const Tournaments: React.FC = () => {
  return (
    <ScreenLayout title="Kicker - Tournaments">
      <div className={cx("tournaments")}>
        <TournamentsHistory />
      </div>
    </ScreenLayout>
  );
};
