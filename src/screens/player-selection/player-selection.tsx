import { useRouter } from "next/router";
import { useState, useEffect, useCallback } from "react";
import styles from "./player-selection.module.css";
import classNames from "classnames/bind";
import { signIn } from "next-auth/client";
import { User, GameSlot, PlayerPosition, TeamPosition } from "src/types";
import { GameSlotTile } from "./game-slot-tile";
import { isTeamPosition } from "src/utils/is-team-position";
import { isPlayerPosition } from "src/utils/is-player-position";
import { postSelectPlayerEvent } from "src/api/post-select-player-event";

const baseURL = process.env.NEXT_PUBLIC_API_HOST;

const cx = classNames.bind(styles);

interface Props {
  user: User | null;
}

export function PlayerSelection({ user }: Props) {
  const router = useRouter();
  const [gameSlots, setGameSlots] = useState<GameSlot[]>();

  useEffect(() => {
    const eventSourceMessageHandler = (event: Event) => {
      const messageEvent = event as MessageEvent;
      const data = messageEvent.data ? JSON.parse(messageEvent.data) : null;
      if (data.type === "update-all-players") {
        setGameSlots(data.payload.gameSlots);
      }
    };
    const gameEventSource = new EventSource(
      `${baseURL}/api/active-game/events`
    );
    gameEventSource.addEventListener("message", eventSourceMessageHandler);
    return () =>
      gameEventSource.removeEventListener("message", eventSourceMessageHandler);
  }, []);

  const handleGameSlotSelect = useCallback(
    (team: TeamPosition, position: PlayerPosition) => {
      const queryParams = new URLSearchParams({
        team: team.toString(),
        position: position.toString(),
      });

      if (user) {
        postSelectPlayerEvent({ team, position, user });
      } else {
        signIn("google", {
          callbackUrl: `${window.location.origin}/player-selection?${queryParams}`,
        });
      }
    },
    [user]
  );

  useEffect(() => {
    const team = Number(router.query.team);
    const position = Number(router.query.position);

    if (isTeamPosition(team) && isPlayerPosition(position)) {
      handleGameSlotSelect(team, position);
      router.replace(router.pathname);
    }
  }, [router.query, router, handleGameSlotSelect]);

  return (
    <div className={cx("player-selection")}>
      <div className={cx("player-selection__center")}></div>
      {gameSlots?.map((gameSlot, index) => {
        return (
          <GameSlotTile
            user={gameSlot.user}
            team={gameSlot.team}
            position={gameSlot.position}
            onSelect={handleGameSlotSelect}
            key={index}
          />
        );
      })}
    </div>
  );
}
