import React from "react";
import { PlayerPosition, TeamPosition, GameSlot } from "src/types";
import styles from "./game-slot-tile.module.css";
import classNames from "classnames/bind";

const cx = classNames.bind(styles);

interface Props {
  user: GameSlot["user"];
  team: TeamPosition;
  position: PlayerPosition;
  onSelect: (team: TeamPosition, position: PlayerPosition) => void;
}

export const GameSlotTile = React.memo(
  ({ user, team, position, onSelect }: Props) => {
    const tileStyle = cx("tile", {
      tile_red: TeamPosition[team] === "Red",
      tile_blue: TeamPosition[team] === "Blue",
      tile_forward: PlayerPosition[position] === "Forward",
      tile_defender: PlayerPosition[position] === "Defender",
    });

    return (
      <div className={tileStyle}>
        <div className={cx("tile__action")}>
          <button
            className={cx(
              "tile__action-button",
              { "tile__action-button_add": !user },
              { "tile__action-button_circle": !user }
            )}
            onClick={() => onSelect(team, position)}
            title="select player position"
            type="button"
          >
            {user && (
              <img
                className={cx("tile__action-button_circle")}
                alt="player's avatar"
                src={user?.photoUrl}
              />
            )}
          </button>
          <span className={cx("tile__action-title")}>
            {user?.name || PlayerPosition[position]}
          </span>
        </div>
      </div>
    );
  }
);
