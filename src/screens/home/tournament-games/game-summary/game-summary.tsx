import React from "react";
import styles from "./game-summary.module.css";
import classNames from "classnames/bind";
import { GameResults } from "src/types";
import { TeamPhoto } from "src/components/team-photo";

const cx = classNames.bind(styles);

interface Props {
  gameResults: GameResults;
}

export const GameSummary: React.FC<Props> = ({ gameResults }) => {
  const teamLeft = gameResults.teamRed;
  const teamRight = gameResults.teamBlue;

  return (
    <div className={cx("game-summary")}>
      <div
        className={cx("game-summary__left-side", {
          "game-summary__left-side_gradient":
            gameResults.goalsRed > gameResults.goalsBlue,
        })}
      >
        <TeamPhoto team={teamLeft} />
        <div className={cx("game-summary__team")}>
          <div className={cx("game-summary__team-name")}>{teamLeft.name}</div>
          <div className={cx("game-summary__players")}>
            {teamLeft.player1.name},<br className={cx("game-summary__break")} />{" "}
            {teamLeft.player2.name}
          </div>
        </div>
        <div className={cx("game-summary__score")}>
          {gameResults.game ? gameResults.goalsRed : "-"}
        </div>
      </div>
      <span className={cx("game-summary__colon")}>:</span>
      <div
        className={cx("game-summary__right-side", {
          "game-summary__right-side_gradient":
            gameResults.goalsBlue > gameResults.goalsRed,
        })}
      >
        <div className={cx("game-summary__score", "game-summary__score_right")}>
          {gameResults.game ? gameResults.goalsBlue : "-"}
        </div>
        <div className={cx("game-summary__team", "game-summary__team_right")}>
          <div className={cx("game-summary__team-name")}>{teamRight.name}</div>
          <div className={cx("game-summary__players")}>
            {teamRight.player1.name},
            <br className={cx("game-summary__break")} />{" "}
            {teamRight.player2.name}
          </div>
        </div>
        <TeamPhoto team={teamRight} />
      </div>
    </div>
  );
};
