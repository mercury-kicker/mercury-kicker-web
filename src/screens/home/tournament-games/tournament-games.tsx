import { useTournamentGamesResults } from "src/api/use-tournament-games-results";
import React from "react";
import styles from "./tournament-games.module.css";
import classNames from "classnames/bind";
import { Tournament } from "src/types";
import { GameSummary } from "./game-summary";

const cx = classNames.bind(styles);

interface Props {
  tournamentId: Tournament["id"];
}

export const TournamentGames: React.FC<Props> = ({ tournamentId }) => {
  const { data, error } = useTournamentGamesResults(tournamentId);

  const gamesResultsFilteredAndSorted = React.useMemo(() => {
    if (data) {
      return data.gamesResults
        .filter((item) => item.game)
        .sort(
          (a, b) =>
            new Date(b.game.createdAt).getTime() -
            new Date(a.game.createdAt).getTime()
        );
    }
  }, [data]);

  if (error || !gamesResultsFilteredAndSorted) return null;
  if (!data && !error) return null;

  return (
    <div className={cx("tournament-games")}>
      <p className={cx("tournament-games__label")}>Матчи</p>
      {gamesResultsFilteredAndSorted.map((item) => (
        <GameSummary gameResults={item} key={item.gameId} />
      ))}
    </div>
  );
};
