import { useTournament } from "src/api/use-tournament";
import format from "date-fns/format";
import styles from "./home.module.css";
import classNames from "classnames/bind";
import { TournamentStats } from "./tournament-stats";
import { ScreenLayout } from "src/components/screen-layout";
import { PlayersStats } from "./players-stats";
import { TournamentGames } from "./tournament-games";

let cx = classNames.bind(styles);

export const Home: React.FC = () => {
  const { data, error } = useTournament();

  if (error) return <div>An error has occurred.</div>;
  if (!data && !error) return <div>Loading...</div>;

  const tournament = data?.tournament;

  if (!tournament) {
    return null;
  }

  return (
    <ScreenLayout title="Kicker">
      <div className={cx("home")}>
        <div className={cx("home__header")}>
          <h2 className={cx("home__tournament-dates")}>
            {format(tournament.startDate, "DD MMM")}
            {" — "}
            {format(tournament.endDate, "DD MMM YYYY")}
          </h2>
          <h1 className={cx("home__tournament-title")}>{tournament.title}</h1>
        </div>
        <TournamentStats tournamentId={tournament.id} />
        <PlayersStats tournamentId={tournament.id} />
        <TournamentGames tournamentId={tournament.id} />
      </div>
    </ScreenLayout>
  );
};
