import { useTournamentStats } from "src/api/use-tournament-stats";
import React, { useCallback } from "react";
import styles from "./players-stats.module.css";
import classNames from "classnames/bind";
import { Cell, Column, ColumnInstance, TableCommonProps } from "react-table";
import { PlayerStats, Tournament } from "src/types";
import { UserPhoto } from "src/components/user-photo";
import { Table } from "src/components/table";

const cx = classNames.bind(styles);

interface Props {
  tournamentId: Tournament["id"];
}

export const PlayersStats: React.FC<Props> = ({ tournamentId }) => {
  const { data, error } = useTournamentStats(tournamentId);

  const columns = React.useMemo(
    (): Column<PlayerStats>[] => [
      {
        id: "user.photo",
        Header: "Player",
        accessor: "photoUrl",
        Cell(cell: Cell) {
          return <UserPhoto index={cell.row.index + 1} photoUrl={cell.value} />;
        },
      },
      {
        id: "user.name",
        Header: "",
        accessor: "name",
        Cell(cell: Cell) {
          return (
            <div className={cx("players-stats__user-name")}>{cell.value}</div>
          );
        },
      },
      {
        id: "user.games",
        Header: "Games",
        accessor: "games",
      },
      {
        id: "user.wins",
        Header: "Wins %",
        accessor: "wins",
      },
      {
        id: "user.goals",
        Header: "Goals avg.",
        accessor: "goals",
      },
      {
        id: "user.keep",
        Header: "Keeps avg.",
        accessor: "keep",
      },
      {
        id: "user.rating",
        Header: "Rating",
        accessor: "rating",
      },
    ],
    []
  );

  const getColumnProps = useCallback(
    (column: ColumnInstance<PlayerStats>): Partial<TableCommonProps> => {
      if (column.id === "user.photo") {
        return { style: { fontWeight: "bolder" } };
      }
      if (column.id === "user.name") {
        return { className: cx("players-stats__cell_player") };
      }
      if (column.id === "user.games") {
        return { className: cx("players-stats__cell_results") };
      }
      if (column.id === "user.wins") {
        return { className: cx("players-stats__cell_results") };
      }
      if (column.id === "user.goals") {
        return { className: cx("players-stats__cell_results") };
      }
      if (column.id === "user.keep") {
        return { className: cx("players-stats__cell_results") };
      }
      if (column.id === "user.rating") {
        return {
          className: cx(
            "players-stats__cell_results",
            "players-stats__cell_rating"
          ),
        };
      }
      return {};
    },
    []
  );

  const getHeaderProps = useCallback(
    (column: ColumnInstance<PlayerStats>): Partial<TableCommonProps> => {
      if (column.id === "user.photo") {
        return {
          className: cx("players-stats__cell_header"),
          style: { fontWeight: "bolder", transform: `rotate(0deg)` },
        };
      }
      if (column.id === "user.games") {
        return { className: cx("players-stats__cell_header") };
      }
      if (column.id === "user.wins") {
        return { className: cx("players-stats__cell_header") };
      }
      if (column.id === "user.goals") {
        return { className: cx("players-stats__cell_header") };
      }
      if (column.id === "user.keep") {
        return { className: cx("players-stats__cell_header") };
      }
      if (column.id === "user.rating") {
        return {
          style: { color: "#ed4159" },
          className: cx("players-stats__cell_header"),
        };
      }
      return {};
    },
    []
  );

  const stats = React.useMemo(() => {
    if (data?.usersStats?.all) {
      return data.usersStats.all;
    }
    return [];
  }, []);

  if (error) return <div>An error has occurred.</div>;
  if (!data && !error) return <div>Loading...</div>;

  return (
    <div>
      <p className={cx("players-stats__title")}>Рейтинг игроков</p>
      <Table
        className={cx("players-stats__table")}
        data={stats}
        columns={columns}
        getColumnProps={getColumnProps}
        getHeaderProps={getHeaderProps}
      />
    </div>
  );
};
