import { useTournamentStats } from "src/api/use-tournament-stats";
import { TeamProgress } from "./team-progress";
import React, { useCallback } from "react";
import styles from "./tournament-stats.module.css";
import classNames from "classnames/bind";
import { TeamStats, Tournament } from "src/types";
import { Column, ColumnInstance, TableCommonProps } from "react-table";
import { TeamPhoto } from "src/components/team-photo";
import { Table } from "src/components/table";

const cx = classNames.bind(styles);

interface Props {
  tournamentId: Tournament["id"];
}

export const TournamentStats: React.FC<Props> = ({ tournamentId }) => {
  const { data, error } = useTournamentStats(tournamentId);

  const columns = React.useMemo(
    (): Column<TeamStats>[] => [
      {
        id: "team.photo",
        Header: "Team",
        accessor: "team",
        Cell({ cell }) {
          return (
            <TeamPhoto
              position={cell.row.index + 1}
              team={cell.value}
              style={{ marginLeft: 10 }}
            />
          );
        },
      },
      {
        id: "team.name",
        Header: "",
        accessor: "team",
        Cell({ cell: { value } }) {
          return (
            <>
              <div className={cx("team__name")}>{value.name}</div>
              <div className={cx("team__players")}>
                {value.player1.name} <br className={cx("team__break")} />
                {value.player2.name}
              </div>
              <TeamProgress teamId={value.id} tournamentId={tournamentId} />
            </>
          );
        },
      },
      {
        id: "team.games",
        Header: "Games",
        accessor: "games",
      },
      {
        id: "team.wins",
        Header: "Wins",
        accessor: "wins",
      },
      {
        id: "team.defeats",
        Header: "Defeats",
        accessor: "defeats",
      },
      {
        id: "team.goalsScored",
        Header: "Goals scored",
        accessor: "goalsScored",
      },
      {
        id: "team.goalsMissed",
        Header: "Goals missed",
        accessor: "goalsMissed",
      },
    ],
    [tournamentId]
  );

  const getColumnProps = useCallback(
    (column: ColumnInstance<TeamStats>): Partial<TableCommonProps> => {
      if (column.id === "team.name") {
        return {
          className: cx("tournament-stats__cell_team"),
        };
      }
      if (column.id === "team.games") {
        return {
          className: cx("tournament-stats__cell_results"),
        };
      }
      if (column.id === "team.wins") {
        return {
          className: cx(
            "tournament-stats__cell_results",
            "tournament-stats__cell_wins"
          ),
        };
      }
      if (column.id === "team.defeats") {
        return {
          className: cx("tournament-stats__cell_results"),
        };
      }
      if (column.id === "team.goalsScored") {
        return {
          className: cx("tournament-stats__cell_results"),
        };
      }
      if (column.id === "team.goalsMissed") {
        return {
          className: cx("tournament-stats__cell_results"),
        };
      }
      return {};
    },
    []
  );

  const getHeaderProps = useCallback(
    (column: ColumnInstance<TeamStats>): Partial<TableCommonProps> => {
      if (column.id === "team.photo") {
        return {
          className: cx("tournament-stats__cell_header"),
          style: { fontWeight: "bolder", transform: `rotate(0deg)` },
        };
      }
      if (column.id === "team.games") {
        return { className: cx("tournament-stats__cell_header") };
      }
      if (column.id === "team.wins") {
        return {
          style: { color: "#ed4159" },
          className: cx("tournament-stats__cell_header"),
        };
      }
      if (column.id === "team.defeats") {
        return { className: cx("tournament-stats__cell_header") };
      }
      if (column.id === "team.goalsScored") {
        return { className: cx("tournament-stats__cell_header") };
      }
      if (column.id === "team.goalsMissed") {
        return { className: cx("tournament-stats__cell_header") };
      }
      return {};
    },
    []
  );

  const standings: TeamStats[] = React.useMemo(
    () =>
      data?.stats?.sort((a: TeamStats, b: TeamStats) => b.wins - a.wins) ?? [],
    [data?.stats]
  );

  if (error) return <div>An error has occurred.</div>;
  if (!data && !error) return <div>Loading...</div>;

  return (
    <div className={cx("tournament-stats")}>
      <p className={cx("tournament-stats__label")}>Рейтинг команд</p>
      <Table
        columns={columns}
        data={standings}
        getColumnProps={getColumnProps}
        getHeaderProps={getHeaderProps}
      />
    </div>
  );
};
