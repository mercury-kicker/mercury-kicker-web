import { useTournamentGamesResults } from "src/api/use-tournament-games-results";
import React from "react";
import styles from "./team-progress.module.css";
import classNames from "classnames/bind";
import { Tournament, Team, GameResults } from "src/types";

const cx = classNames.bind(styles);

interface Props {
  tournamentId: Tournament["id"];
  teamId: Team["id"];
}

export const TeamProgress: React.FC<Props> = ({ tournamentId, teamId }) => {
  const { data, error } = useTournamentGamesResults(tournamentId);

  const isTeamWin = React.useCallback(
    (gameResults: GameResults): boolean => {
      if (gameResults.teamRed.id === teamId) {
        return gameResults.goalsRed > gameResults.goalsBlue;
      }
      if (gameResults.teamBlue.id === teamId) {
        return gameResults.goalsBlue > gameResults.goalsRed;
      }
      return false;
    },
    [teamId]
  );

  const gamesResultsFilteredAndSorted = React.useMemo(() => {
    if (data) {
      return data.gamesResults
        .filter((item) => item.game)
        .filter(
          (item) => item.teamRed.id === teamId || item.teamBlue.id === teamId
        )
        .sort(
          (a, b) =>
            new Date(a.game.createdAt).getTime() -
            new Date(b.game.createdAt).getTime()
        );
    }
  }, [data, teamId]);

  if (error || !gamesResultsFilteredAndSorted) return null;
  if (!data && !error) return null;

  return (
    <div className={cx("team-progress")}>
      {gamesResultsFilteredAndSorted.map((item, index) => {
        return (
          <div
            className={cx("team-progress__dot", {
              "team-progress__dot_win": isTeamWin(item),
            })}
            key={index}
          />
        );
      })}
    </div>
  );
};
