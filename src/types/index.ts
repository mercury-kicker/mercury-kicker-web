export interface User {
  id: number;
  name: string;
  photoUrl: string;
  email?: string;
  rating: number;
  createdAt: string;
  updatedAt: string;
  GamePlayer?: GamePlayer;
}

export interface GamePlayer {
  team: number;
  position: number;
  createdAt: string;
  updatedAt: string;
  GameId: Game["id"];
  UserId: User["id"];
}

export interface Team {
  createdAt: string;
  id: number;
  name: string;
  player1: User;
  player1Id: User["id"];
  player2: User;
  player2Id: User["id"];
  updatedAt: string;
}

export interface TeamStats {
  defeats: number;
  games: number;
  goalsMissed: number;
  goalsScored: number;
  team: Team;
  teamId: Team["id"];
  wins: number;
}

export interface Tournament {
  id: number;
  title: string;
  startDate: string;
  endDate: string;
  status: null;
  createdAt: string;
  updatedAt: string;
  TournamentGames: TournamentGame[];
}

export interface TournamentGame {
  id: number;
  tournamentId: Tournament["id"];
  team1Id: Team["id"];
  team2Id: Team["id"];
  createdAt: string;
  updatedAt: string;
  TournamentId: Tournament["id"];
  team1: Team;
  team2: Team;
}

export enum TeamPosition {
  Red = 0,
  Blue = 1,
}

export enum PlayerPosition {
  Forward = 0,
  Defender = 1,
}

export interface GameSlot {
  team: TeamPosition;
  position: PlayerPosition;
  user: User | null;
}

export interface PlayerStats {
  id: User["id"];
  name: User["name"];
  photoUrl: User["photoUrl"];
  defeats: number;
  games: number;
  goals: number;
  keep: number;
  rating: number;
  wins: number;
}

export interface GameResults {
  tournamentGameId: number;
  team1Id: Team["id"];
  team2Id: Team["id"];
  gameId: Game["id"];
  teamIdRed: Team["id"];
  teamIdBlue: Team["id"];
  goalsRed: number;
  goalsBlue: number;
  game: Game;
  team1: Team;
  team2: Team;
  teamRed: Team;
  teamBlue: Team;
}

export interface Game {
  id: number;
  ball: null;
  status: null;
  createdAt: string;
  updatedAt: string;
  Users: User[];
  Goals: Goal[];
}

export interface Goal {
  id: number;
  ownGoal: boolean;
  createdAt: string;
  updatedAt: string;
  GameId: Game["id"];
  UserId: User["id"];
}

export interface TournamentStats {
  stats: TeamStats[];
  usersStats: {
    all: PlayerStats[];
    forwards: PlayerStats[];
    defenders: PlayerStats[];
  };
}

export interface TournamentSummary {
  id: Tournament["id"];
  title: Tournament["title"];
  startDate: Tournament["startDate"];
  endDate: Tournament["endDate"];
  winnerTeam: Team | null;
}
